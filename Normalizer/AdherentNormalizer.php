<?php

namespace SGDF\IntranetApiBundle\Normalizer;

use Doctrine\ORM\EntityManagerInterface;
use SGDF\IntranetApiBundle\Entity\Adherent;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;
use Symfony\Component\PropertyInfo\PropertyTypeExtractorInterface;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactoryInterface;
use Symfony\Component\Serializer\NameConverter\NameConverterInterface;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class AdherentNormalizer extends ObjectNormalizer
{
    /**
     * Entity manager
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * Entity normalizer
     * @param EntityManagerInterface $em
     * @param ClassMetadataFactoryInterface|null $classMetadataFactory
     * @param NameConverterInterface|null $nameConverter
     * @param PropertyAccessorInterface|null $propertyAccessor
     * @param PropertyTypeExtractorInterface|null $propertyTypeExtractor
     */
    public function __construct(
        EntityManagerInterface $em,
        ?ClassMetadataFactoryInterface $classMetadataFactory = null,
        ?NameConverterInterface $nameConverter = null,
        ?PropertyAccessorInterface $propertyAccessor = null,
        ?PropertyTypeExtractorInterface $propertyTypeExtractor = null
    ) {
        parent::__construct($classMetadataFactory, $nameConverter, $propertyAccessor, $propertyTypeExtractor);

        // Entity manager
        $this->em = $em;
    }

    /**
     * @inheritDoc
     */
    public function supportsNormalization($data, $format = null)
    {
        return false;
    }

    /**
     * @inheritDoc
     */
    public function supportsDenormalization($data, $type, $format = null)
    {
        return ($type === "SGDF\IntranetApiBundle\Entity\Adherent");
    }

    /**
     * @inheritDoc
     */
    public function denormalize($data, $class, $format = null, array $context = [])
    {
        $adherent = $this->em->find($class, $data['codeAdherent']);

        if ($adherent == null){
            $adherent = new Adherent();
        } 
        else {
            // Get adresse and fonctionPrincipale from db
            $adresse = $adherent->getAdresse();
            $fonction = $adherent->getFonctionPrincipale();
            // Fill it with intranet data
            $adresse->fromArray($data['adresse']);
            $fonction->fromArray($data['fonctionPrincipale']);
        }

        $context[AbstractNormalizer::OBJECT_TO_POPULATE] =  $adherent;
        parent::denormalize($data, $class, $format, $context);

        // If adresse already exist, affect it to adherent
        if (isset($adresse))
            $adherent->setAdresse($adresse);
        if (isset($fonction)){
            // Replace with denormalize intranet data keeping actual db id
            $fonction->setStructure($adherent->getFonctionPrincipale()->getStructure());
            $fonction->setFonction($adherent->getFonctionPrincipale()->getFonction());

            $adherent->setFonctionPrincipale($fonction);
        }

        return $adherent;
    }
}
