<?php
namespace SGDF\IntranetApiBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('intranet_api');
        $rootNode = $treeBuilder->getRootNode();
        
        $rootNode
            ->children()
                ->scalarNode('endpoint')
                    ->defaultValue('%env(SGDF_INTRANETAPI_ENDPOINT)%')->end()
                ->scalarNode('client_id')
                    ->defaultValue('%env(SGDF_INTRANETAPI_CLIENTID)%')->end()
                ->scalarNode('audience')
                    ->defaultValue('%env(SGDF_INTRANETAPI_AUDIENCE)%')->end()
        ->end();

        return $treeBuilder;
    }
}