<?php
namespace SGDF\IntranetApiBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\Loader\XmlFileLoader;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;

class IntranetApiExtension extends Extension implements PrependExtensionInterface
{
    public function prepend(ContainerBuilder $container)
    {
        foreach ($container->getExtensions() as $name => $extension) {
            switch ($name) {
                case 'twig':
                    $config = [
                        'globals' => [
                            'app_version' => '%env(APP_VERSION)%',
                            'app_title' => '%env(APP_TITLE)%'
                        ]
                    ];

                    $container->prependExtensionConfig($name, $config);
                    break;
                case 'security':
                    $config = [
                        'encoders' => [
                            'SGDF\IntranetApiBundle\Entity\User' => [
                                'algorithm' => 'auto'
                            ]
                        ],
                        'providers' => [
                            'sgdf_user_provider' => [
                                'entity' => [
                                    'class' => 'SGDF\IntranetApiBundle\Entity\User',
                                    'property' => 'username'
                                ]
                            ]
                        ],
                        'firewalls' => [
                            'dev' => [
                                'pattern' => '^/(_(profiler|wdt)|css|images|js)/',
                                'security' => false
                            ],
                            'main' => [
                                'anonymous' => 'lazy',
                                'provider' => 'sgdf_user_provider',
                                'guard' => [ 'authenticators' => ['intranet_api.authenticator']],
                                'form_login' => [
                                    'login_path' => 'sgdf_intranetapi_login',
                                    'check_path' => 'sgdf_intranetapi_login',
                                    'csrf_token_generator' => 'security.csrf.token_manager',
                                    'default_target_path' => 'home',
                                    'use_referer' => true
                                ],
                                'remember_me' => [
                                    'token_provider' => 'Symfony\Bridge\Doctrine\Security\RememberMe\DoctrineTokenProvider',
                                    'secret' => '%kernel.secret%',
                                    'lifetime' => 1799,
                                    'path' => '/',
                                ],
                                'logout' => [
                                    'path' => 'sgdf_intranetapi_logout',
                                    'target' => 'sgdf_intranetapi_login'
                                ]
                            ]
                        ],
                        'access_control' => [
                            ['path' => '^/login', 'roles' => 'IS_AUTHENTICATED_ANONYMOUSLY'],
                            ['path' => '^/(css|js)', 'roles' => 'IS_AUTHENTICATED_ANONYMOUSLY'],
                            ['path' => '^/(_wdt|_profiler)', 'roles' => 'IS_AUTHENTICATED_ANONYMOUSLY'],
                            ['path' => '^/admin', 'roles' => 'ROLE_ADMIN'],
                            ['path' => '^/', 'roles' => 'ROLE_USER'],
                        ]
                    ];
                    $container->loadFromExtension('security', $config);
                    break;
            }
        }
    }

    public function load(array $configs, ContainerBuilder $container)
    {
        $loader = new XmlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.xml');

        $configuration = $this->getConfiguration($configs, $container);
        $config = $this->processConfiguration($configuration, $configs);
        
        $definition = $container->getDefinition('intranet_api.authenticator');
        
        $definition->setArgument(0, $config['endpoint']);
        $definition->setArgument(1, $config['client_id']);
        $definition->setArgument(2, $config['audience']);
    }
}