<?php

namespace SGDF\IntranetApiBundle\Security;

use SGDF\IntranetApiBundle\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpClient\Exception\TransportException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Component\Security\Guard\Authenticator\AbstractFormLoginAuthenticator;
use Symfony\Component\Security\Http\Util\TargetPathTrait;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use League\OAuth2\Client\Token\AccessToken;
use SGDF\IntranetApiBundle\Entity\Adherent;
use SGDF\IntranetApiBundle\Entity\Fonction;
use SGDF\IntranetApiBundle\Entity\Lieu;
use SGDF\IntranetApiBundle\Entity\Structure;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;

class LoginFormAuthenticator extends AbstractFormLoginAuthenticator
{
    use TargetPathTrait;

    public const LOGIN_ROUTE = 'sgdf_intranetapi_login';

    private $entityManager;
    private $urlGenerator;
    private $csrfTokenManager;
    private $passwordEncoder;
    private $client;
    private $endpoint;
    private $client_id;
    private $audience;
    private $session;
    private $serializer;

    public function __construct(
        string $endpoint,
        string $client_id,
        string $audience,
        EntityManagerInterface $entityManager,
        UrlGeneratorInterface $urlGenerator,
        CsrfTokenManagerInterface $csrfTokenManager,
        UserPasswordEncoderInterface $passwordEncoder,
        HttpClientInterface $client,
        RequestStack $requestStack,
        SessionInterface $session,
        SerializerInterface $serializer
    ) {
        $this->entityManager = $entityManager;
        $this->urlGenerator = $urlGenerator;
        $this->csrfTokenManager = $csrfTokenManager;
        $this->passwordEncoder = $passwordEncoder;
        $this->client = $client;
        $this->requestStack = $requestStack;
        $this->endpoint = $endpoint;
        $this->client_id = $client_id;
        $this->audience = $audience;
        $this->session = $session;
        $this->serializer = $serializer;
    }

    public function supports(Request $request)
    {
        return self::LOGIN_ROUTE === $request->attributes->get('_route')
            && $request->isMethod('POST');
    }

    public function getCredentials(Request $request)
    {
        $credentials = [
            'username' => $request->request->get('username'),
            'password' => $request->request->get('password'),
            'csrf_token' => $request->request->get('_csrf_token'),
        ];
        $request->getSession()->set(
            Security::LAST_USERNAME,
            $credentials['username']
        );

        return $credentials;
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        $token = new CsrfToken('authenticate', $credentials['csrf_token']);
        if (!$this->csrfTokenManager->isTokenValid($token)) {
            throw new InvalidCsrfTokenException();
        }

        // Authentification auprès de l'intranet
        try {
            $response = $this->client->request(
                'POST',
                $this->endpoint . 'oauth2/token',
                [
                    'body' => [
                        'grant_type' => 'password',
                        'audience' => $this->audience,
                        'client_id' => $this->client_id,
                        'username' => $credentials['username'],
                        'password' => $credentials['password']
                    ]
                ]
            );
            if ($response->getStatusCode() !== 200) {
                $content = $response->toArray(false);
                throw new CustomUserMessageAuthenticationException($content['error_description']);
            } else {
                $content = $response->toArray();
                $accessToken = new AccessToken($content);
                $this->session->set('access_token', $accessToken);

                // récupération du user
                $response = $this->client->request(
                    'GET',
                    $this->endpoint . 'api/v1/adherents/' . $credentials['username'],
                    [
                        'headers' => [
                            'Authorization' => $accessToken->getValues()['token_type'] . ' ' . $accessToken->getToken(),
                            'idAppelant' => $this->client_id
                        ]
                    ]
                );
                if ($response->getStatusCode() !== 200) {
                    //@TODO faire une connexion locale si jamais l'intranet est KO
                    $content = $response->toArray(false);
                    throw new CustomUserMessageAuthenticationException($content['errors'][0]['details'] . " (api/v1/adherents/" . $credentials['username'] . ")");
                } else {
                    // Mise à jour ou création de l'utilisateur
                    $content = $response->toArray();
                    if (isset($content['errors']) && count($content['errors'])>0) {
                        throw new CustomUserMessageAuthenticationException($content['errors'][0]['details'] . " (api/v1/adherents/" . $credentials['username'] . ")");
                    }

                    // L'utilisateur existe-t-il en base ?
                    $user = $this->entityManager->getRepository(User::class)->findOneBy(['adherent' => $credentials['username']]);
                    if (!$user) {
                        $user = new User($content['data']);
                    }

                    // Récupération de l'adhérent depuis la réponse de l'Intranet
                    $data = $this->serializer->serialize($content['data'], 'json');
                    $adherent = $this->serializer->deserialize($data, Adherent::class, 'json', [
                        AbstractObjectNormalizer::DISABLE_TYPE_ENFORCEMENT => true
                    ]);

                    // Association de l'adhérent à l'utilisateur
                    $user->setAdherent($adherent);

                    $user->setEmail($user->getAdherent()->getEmails()[0])
                        ->updateLastLoginTime()
                        ->setPassword($this->passwordEncoder->encodePassword($user, $credentials['password']))
                        ->setRoles($user->getRoles());

                    // Création ou mise à jour des données
                    $this->entityManager->persist($user);
                    $this->entityManager->flush();

                    return $user;
                }
            }
        } catch (TransportException $e) {
            $user = $this->entityManager->getRepository(User::class)->findOneBy(['adherent' => $credentials['username']]);
            if ($user) {
                $this->session->getFlashBag()->add('warning', 'Intranet injoignable - Connexion locale');
                return $user;
            } else {
                throw new CustomUserMessageAuthenticationException("L'application n'est pas parvenue à se connecter à l'Intranet SGDF. Identifiant inconnu localement.");
            }
        }
    }

    public function checkCredentials($credentials, UserInterface $user)
    {
        return $this->passwordEncoder->isPasswordValid($user, $credentials['password']);
        return true;
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        if ($targetPath = $this->getTargetPath($request->getSession(), $providerKey)) {
            return new RedirectResponse($targetPath);
        }

        return new RedirectResponse($this->urlGenerator->generate('home'));
    }

    protected function getLoginUrl()
    {
        return $this->urlGenerator->generate(self::LOGIN_ROUTE);
    }
}
