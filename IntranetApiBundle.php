<?php
namespace SGDF\IntranetApiBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * SGDF Intranet API Bundle
 * 
 * @author Eric Perrodon <eric@perrodon.fr>
 */
class IntranetApiBundle extends Bundle
{
    public function build(ContainerBuilder $container){
        parent::build($container);
    }
}
