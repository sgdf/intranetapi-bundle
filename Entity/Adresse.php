<?php

namespace SGDF\IntranetApiBundle\Entity;

use SGDF\IntranetApiBundle\Repository\AdresseRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AdresseRepository::class)
 */
class Adresse
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ligne1;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ligne2;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ligne3;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $codePostal;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ville;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $pays;

    /**
     * @ORM\Column(type="boolean")
     */
    private $npai;

    /**
     * @ORM\OneToOne(targetEntity=Adherent::class, mappedBy="adresse")
     */
    private $adherent;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLigne1(): ?string
    {
        return $this->ligne1;
    }

    public function setLigne1(?string $ligne1): self
    {
        $this->ligne1 = $ligne1;

        return $this;
    }

    public function getLigne2(): ?string
    {
        return $this->ligne2;
    }

    public function setLigne2(?string $ligne2): self
    {
        $this->ligne2 = $ligne2;

        return $this;
    }

    public function getLigne3(): ?string
    {
        return $this->ligne3;
    }

    public function setLigne3(?string $ligne3): self
    {
        $this->ligne3 = $ligne3;

        return $this;
    }

    public function getCodePostal(): ?string
    {
        return $this->codePostal;
    }

    public function setCodePostal(?string $codePostal): self
    {
        $this->codePostal = $codePostal;

        return $this;
    }

    public function getVille(): ?string
    {
        return $this->ville;
    }

    public function setVille(?string $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    public function getPays(): ?string
    {
        return $this->pays;
    }

    public function setPays(?string $pays): self
    {
        $this->pays = $pays;

        return $this;
    }

    public function getNpai(): ?bool
    {
        return $this->npai;
    }

    public function setNpai(bool $npai): self
    {
        $this->npai = $npai;

        return $this;
    }

    public function getAdherent(): ?Adherent
    {
        return $this->adherent;
    }

    public function setAdherent(Adherent $adherent): self
    {
        // set the owning side of the relation if necessary
        if ($adherent->getAdresse() !== $this) {
            $adherent->setAdresse($this);
        }

        $this->adherent = $adherent;

        return $this;
    }

    // /**
    //  * @return array
    //  */
    // public function asArray()
    // {
    //     $properties = [];
    //     foreach ($this as $name => $value) {
    //         $properties[$name] = $value;
    //     }
    //     return $properties;
    // }

    /**
     * @param $array
     */
    public function fromArray($array)
    {
        foreach ($array as $name => $value) {
            if (!isset($this->$name) || $name == 'id') {
                continue;
            }
            $this->$name = $value;
        }
    }
}
