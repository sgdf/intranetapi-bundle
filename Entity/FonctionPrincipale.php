<?php

namespace SGDF\IntranetApiBundle\Entity;

use SGDF\IntranetApiBundle\Repository\FonctionPrincipaleRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=FonctionPrincipaleRepository::class)
 */
class FonctionPrincipale
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Structure::class, cascade={"persist"})
     * @ORM\JoinColumn(name="code_structure", referencedColumnName="code")
     */
    private $structure;

    /**
     * @ORM\ManyToOne(targetEntity=Fonction::class, cascade={"persist"})
     * @ORM\JoinColumn(name="code_fonction", referencedColumnName="code")
     */
    private $fonction;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStructure(): ?Structure
    {
        return $this->structure;
    }

    public function setStructure(?Structure $structure): self
    {
        $this->structure = $structure;

        return $this;
    }

    public function getFonction(): ?Fonction
    {
        return $this->fonction;
    }

    public function setFonction(?Fonction $fonction): self
    {
        $this->fonction = $fonction;

        return $this;
    }

    /**
     * @param $array
     */
    public function fromArray($array)
    {
        foreach ($array as $name => $value) {
            if (!isset($this->$name) || $name == 'id') {
                continue;
            }
            $this->$name = $value;
        }
    }
}
