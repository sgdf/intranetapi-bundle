<?php

namespace SGDF\IntranetApiBundle\Entity;

use SGDF\IntranetApiBundle\Repository\StructureRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=StructureRepository::class)
 */
class Structure
{

    /**
     * @ORM\Column(type="integer")
     */
    private $idInvariantStructure;

    /**
     * @ORM\Id
     * @ORM\Column(type="string", length=50)
     */
    private $code;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $echelon;

    public function getIdInvariantStructure(): ?int
    {
        return $this->idInvariantStructure;
    }

    public function setIdInvariantStructure(int $idInvariantStructure): self
    {
        $this->idInvariantStructure = $idInvariantStructure;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getEchelon(): ?string
    {
        return $this->echelon;
    }

    public function setEchelon(string $echelon): self
    {
        $this->echelon = $echelon;

        return $this;
    }
}
