<?php

namespace SGDF\IntranetApiBundle\Entity;

use SGDF\IntranetApiBundle\Repository\AdherentRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AdherentRepository::class)
 */
class Adherent
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     */
    private $codeAdherent;
    
    /**
     * @ORM\Column(type="integer")
     */
    private $idInscription;

    /**
     * @ORM\Column(type="smallint")
     */
    private $idCivilite;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $saison;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nomNaissance;

    /**
     * @ORM\Column(type="date")
     */
    private $dateDeNaissance;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $statut;

    /**
     * @ORM\Column(type="boolean")
     */
    private $droitImage;

    /**
     * @ORM\Column(type="boolean")
     */
    private $droitsInformations;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $prenom;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $telephoneBureau;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $telephoneDomicile;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $telephonePortable1;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $telephonePortable2;

    /**
     * @ORM\OneToOne(targetEntity=Adresse::class, inversedBy="adherent", cascade={"persist", "remove"}, orphanRemoval=true)
     * @ORM\JoinColumn(nullable=false)
     */
    private $adresse;

    /**
     * @ORM\OneToOne(targetEntity=FonctionPrincipale::class, cascade={"persist", "remove"}, orphanRemoval=true)
     * @ORM\JoinColumn(nullable=false)
     */
    private $fonctionPrincipale;

    /**
     * @ORM\Column(type="array")
     */
    private $emails = [];

    /**
     * @ORM\ManyToOne(targetEntity=Lieu::class, inversedBy="adherents", cascade={"persist"})
     * @ORM\JoinColumn(referencedColumnName="code_insee")
     */
    private $lieuDeNaissance;

    /**
     * @ORM\OneToOne(targetEntity=User::class, mappedBy="adherent", cascade={"persist", "remove"})
     */
    private $idUser;

    public function getIdCivilite(): ?int
    {
        return $this->idCivilite;
    }

    public function setIdCivilite(int $idCivilite): self
    {
        $this->idCivilite = $idCivilite;

        return $this;
    }

    public function getIdInscription(): ?int
    {
        return $this->idInscription;
    }

    public function setIdInscription(int $idInscription): self
    {
        $this->idInscription = $idInscription;

        return $this;
    }

    public function getSaison(): ?string
    {
        return $this->saison;
    }

    public function setSaison(string $saison): self
    {
        $this->saison = $saison;

        return $this;
    }

    public function getNomNaissance(): ?string
    {
        return $this->nomNaissance;
    }

    public function setNomNaissance(string $nomNaissance): self
    {
        $this->nomNaissance = $nomNaissance;

        return $this;
    }

    public function getDateDeNaissance(): ?\DateTimeInterface
    {
        return $this->dateDeNaissance;
    }

    public function setDateDeNaissance(\DateTimeInterface $dateDeNaissance): self
    {
        $this->dateDeNaissance = $dateDeNaissance;

        return $this;
    }

    public function getStatut(): ?string
    {
        return $this->statut;
    }

    public function setStatut(string $statut): self
    {
        $this->statut = $statut;

        return $this;
    }

    public function getDroitImage(): ?bool
    {
        return $this->droitImage;
    }

    public function setDroitImage(bool $droitImage): self
    {
        $this->droitImage = $droitImage;

        return $this;
    }

    public function getDroitsInformations(): ?bool
    {
        return $this->droitsInformations;
    }

    public function setDroitsInformations(bool $droitsInformations): self
    {
        $this->droitsInformations = $droitsInformations;

        return $this;
    }

    public function getCodeAdherent(): ?int
    {
        return $this->codeAdherent;
    }

    public function setCodeAdherent(int $codeAdherent): self
    {
        $this->codeAdherent = $codeAdherent;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getTelephoneBureau(): ?string
    {
        return $this->telephoneBureau;
    }

    public function setTelephoneBureau(?string $telephoneBureau): self
    {
        $this->telephoneBureau = $telephoneBureau;

        return $this;
    }

    public function getTelephoneDomicile(): ?string
    {
        return $this->telephoneDomicile;
    }

    public function setTelephoneDomicile(?string $telephoneDomicile): self
    {
        $this->telephoneDomicile = $telephoneDomicile;

        return $this;
    }

    public function getTelephonePortable1(): ?string
    {
        return $this->telephonePortable1;
    }

    public function setTelephonePortable1(?string $telephonePortable1): self
    {
        $this->telephonePortable1 = $telephonePortable1;

        return $this;
    }

    public function getTelephonePortable2(): ?string
    {
        return $this->telephonePortable2;
    }

    public function setTelephonePortable2(?string $telephonePortable2): self
    {
        $this->telephonePortable2 = $telephonePortable2;

        return $this;
    }

    public function getAdresse(): ?Adresse
    {
        return $this->adresse;
    }

    public function setAdresse(Adresse $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getFonctionPrincipale(): ?FonctionPrincipale
    {
        return $this->fonctionPrincipale;
    }

    public function setFonctionPrincipale(FonctionPrincipale $fonctionPrincipale): self
    {
        $this->fonctionPrincipale = $fonctionPrincipale;

        return $this;
    }

    public function getEmails(): ?array
    {
        return $this->emails;
    }

    public function setEmails(array $emails): self
    {
        $this->emails = $emails;

        return $this;
    }

    public function getLieuDeNaissance(): ?Lieu
    {
        return $this->lieuDeNaissance;
    }

    public function setLieuDeNaissance(?Lieu $lieuDeNaissance): self
    {
        $this->lieuDeNaissance = $lieuDeNaissance;

        return $this;
    }
}
