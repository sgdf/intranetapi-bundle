<?php

namespace SGDF\IntranetApiBundle\Repository;

use SGDF\IntranetApiBundle\Entity\Adherent;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Adherent|null find($id, $lockMode = null, $lockVersion = null)
 * @method Adherent|null findOneBy(array $criteria, array $orderBy = null)
 * @method Adherent[]    findAll()
 * @method Adherent[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AdherentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Adherent::class);
    }

    /**
     * @return Adherent[] Returns an array of Adherent objects
     */
    public function getResponsablesFromStructure($codeStructure)
    {
        return $this->createQueryBuilder('adherent')
            ->leftJoin('adherent.fonctionPrincipale', 'fonctionPrincipale')
            ->leftJoin('fonctionPrincipale.structure', 'structure')
            ->andWhere('structure.code = :code_structure')
            ->andWhere('fonctionPrincipale.fonction >= :code_fonction')
            ->setParameter('code_structure', $codeStructure)
            ->setParameter('code_fonction', 200)
            ->orderBy('adherent.nom', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return Adherent[] Returns an array of Adherent objects
     */
    public function getJeunesFromStructure($codeStructure)
    {
        return $this->createQueryBuilder('adherent')
            ->leftJoin('adherent.fonctionPrincipale', 'fonctionPrincipale')
            ->leftJoin('fonctionPrincipale.structure', 'structure')
            ->andWhere('structure.code = :code_structure')
            ->andWhere('fonctionPrincipale.fonction < :code_fonction')
            ->setParameter('code_structure', $codeStructure)
            ->setParameter('code_fonction', 200)
            ->orderBy('adherent.nom', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

    /*
    public function findOneBySomeField($value): ?Adherent
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
