<?php

namespace SGDF\IntranetApiBundle\Repository;

use SGDF\IntranetApiBundle\Entity\FonctionPrincipale;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method FonctionPrincipale|null find($id, $lockMode = null, $lockVersion = null)
 * @method FonctionPrincipale|null findOneBy(array $criteria, array $orderBy = null)
 * @method FonctionPrincipale[]    findAll()
 * @method FonctionPrincipale[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FonctionPrincipaleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FonctionPrincipale::class);
    }

    // /**
    //  * @return FonctionPrincipale[] Returns an array of FonctionPrincipale objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?FonctionPrincipale
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
