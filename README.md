Installation
============

Make sure Composer is installed globally, as explained in the
[installation chapter](https://getcomposer.org/doc/00-intro.md)
of the Composer documentation.

### Step 0: Add Repository in your composer.json

Open composer.json, define the gitlab repository of this bundle:

```json
    "repositories": [
        {
            "type": "vcs",
            "url" : "https://gitlab.com/sgdf/intranetapi-bundle"
        }
    ],
```

### Step 1: Download the Bundle

Open a command console, enter your project directory and execute the
following command to download the latest stable version of this bundle:

```console
$ composer require sgdf/intranetapi-bundle
```

### Step 2: Enable the Bundle (generated automatically by Flex)

Then, enable the bundle by adding it to the list of registered bundles
in the `config/bundles.php` file of your project:

```php
// config/bundles.php

return [
    // ...
    SGDF\IntranetApiBundle\IntranetApiBundle::class => ['all' => true],
];
```

### Step 3: Declare firewalls

Then, enable authentication by adding firewalls to the list of firewalls
in the `config/packages/security.yaml` file of your project:

```yaml
# config/packages/security.yaml

security:
    firewalls:
        dev:
        main:
```

Nothing else to add, all firewalls configuration are in the bundle.

### Step 4: Add routes

Then, enable the bundle routes by adding them to the list of routes
in the `config/routes.yaml` file of your project:

```yaml
#config/routes.yaml

sgdf_intranetapi:
    # loads routes from the YAML, XML or PHP files found in some bundle directory
    resource: '@IntranetApiBundle/Resources/config/routing/'
    type:     directory
```
